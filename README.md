# hotdog
A pocket pussy with depth sensing.

This repo contains the schematic, 3D prints and Arduino code required to build a Hotdog.

Thanks to [ManlyMarco](https://github.com/ManlyMarco) for coming up with the idea.

## [Demo (NSFW)](https://www.erome.com/a/rvRyi8gS)

## [How to make one (wireless)](doc/wireless/ASSEMBLY.md)

## [How to make one (eco)](doc/eco/ASSEMBLY.md)
